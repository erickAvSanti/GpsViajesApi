var express = require('express');
var app = express();
var db = require('./db');

var VehicleBrandController = require('./utils/VehicleBrandController');
app.use('/vehicles-brand', VehicleBrandController); 

var VehicleModelController = require('./utils/VehicleModelController');
app.use('/vehicles-model', VehicleModelController); 

var PolicyInsuranceCarrierController = require('./utils/PolicyInsuranceCarrierController');
app.use('/policy-insurance-carrier', PolicyInsuranceCarrierController); 

var PolicyEndController = require('./utils/PolicyEndController');
app.use('/policy-end', PolicyEndController); 

var PolicyVehCoverController = require('./utils/PolicyVehCoverController');
app.use('/policy-veh-cover', PolicyVehCoverController); 

var PolicyVehStatusQuotaController = require('./utils/PolicyVehStatusQuotaController');
app.use('/policy-veh-status-quota', PolicyVehStatusQuotaController); 

var PolicyHealthStatusQuotaController = require('./utils/PolicyHealthStatusQuotaController');
app.use('/policy-health-status-quota', PolicyHealthStatusQuotaController); 

var PolicyVehReasonCancelController = require('./utils/PolicyVehReasonCancelController');
app.use('/policy-veh-reason-cancel', PolicyVehReasonCancelController);

var PolicyHealthReasonCancelController = require('./utils/PolicyHealthReasonCancelController');
app.use('/policy-health-reason-cancel', PolicyHealthReasonCancelController); 

var VehicleProdController = require('./utils/VehicleProdController');
app.use('/vehicle-prod', VehicleProdController); 

var VehicleConcesController = require('./utils/VehicleConcesController');
app.use('/vehicle-conces', VehicleConcesController); 

var VehicleConcesLocalController = require('./utils/VehicleConcesLocalController');
app.use('/vehicle-conces-local', VehicleConcesLocalController); 

var VehicleConcesLocalSellerController = require('./utils/VehicleConcesLocalSellerController');
app.use('/vehicle-conces-local-seller', VehicleConcesLocalSellerController); 

var VehicleProvGpsController = require('./utils/VehicleProvGpsController');
app.use('/vehicle-prov-gps', VehicleProvGpsController); 
var VehicleUseController = require('./utils/VehicleUseController');
app.use('/vehicle-use', VehicleUseController); 

var VehicleClassController = require('./utils/VehicleClassController');
app.use('/vehicle-class', VehicleClassController); 

var UserAreaController = require('./utils/UserAreaController');
app.use('/user-area', UserAreaController); 

var CivilStatusController = require('./utils/CivilStatusController');
app.use('/civil-status', CivilStatusController); 

var TipoDocController = require('./utils/TipoDocController');
app.use('/tipo-doc', TipoDocController); 

var AreaAController = require('./areas/AreaAController');
app.use('/area-a', AreaAController); 

var AreaBController = require('./areas/AreaBController');
app.use('/area-b', AreaBController); 

var AreaCController = require('./areas/AreaCController');
app.use('/area-c', AreaCController); 

var ClientController = require('./client/ClientController');
app.use('/clients', ClientController); 


var ContactController = require('./client/ContactController');
app.use('/clients-contacts', ContactController); 


var UserController = require('./user/UserController');
app.use('/users', UserController);


var UserRolController = require('./user/UserRolController');
app.use('/user-roles', UserRolController); 

// app.js
var AuthController = require('./auth/AuthController');
app.use('/api/auth', AuthController);
module.exports = app;

/*

            var _vparam = {};
            for(var idx in req){
                var _param = req[idx];
                var _tparam = typeof _param;
                if( _tparam!="object"){
                    _vparam[idx] = _param;
                }else{
                    if(idx=="query"){
                        _vparam[idx] = _param;
                    }else{
                        _vparam[idx] = "type["+typeof _param+"]";
                    }
                    
                }
            }
            console.log(JSON.stringify(_vparam));
*/