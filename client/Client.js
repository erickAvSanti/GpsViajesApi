var mongoose = require('mongoose');
var moment = require('moment');
var _props = {  
    _id: mongoose.Schema.Types.ObjectId,
    name: {type:String,required:true},
    last_name: {type:String,required:false},
    mother_last_name: {type:String,required:false}, 
    doc_type: {type:String,required:true}, 
    personality: {type:String,required:true}, 
    description: {type:String,required:false}, 
    birthday: {type:Date,required:false}, 
    document: {type:String,required:true,unique:true,index:true}, 
    contacts:[{ type: mongoose.Schema.Types.ObjectId, ref: 'ClientContact' }],
};
var ClientSchema = new mongoose.Schema(_props,
{ 
    timestamps: { 
        createdAt: 'created_at',
        updatedAt:'updated_at' 
    }
});
ClientSchema.pre('save', function(next) {
    next();
}); 
ClientSchema.statics.props = _props;
var _dateFormat = function(str){
    return /^\d{2}\/\d{2}\/\d{4}$/.test(str);
};
ClientSchema.statics.isDateFormat = _dateFormat;
ClientSchema.statics.makeFrom = function(obj){
    var _data = {};
    for(var idx in _props){ 
        if(idx in obj){
            var _prop = obj[idx];
            if(idx=='birthday'){
                if(_prop!='' && _dateFormat(_prop)){
                    _data[idx] = new Date(moment(_prop,"DD/MM/YYYY").format("YYYY-MM-DD"));
                } 
            }else if(typeof _prop != "object" && _prop!=''){
                _data[idx] = _prop;
            }
        } 
    }
    return _data;
};
ClientSchema.statics.fillFrom = function(model,obj){ 
    for(var idx in _props){ 
        if(idx in obj){
            var _prop = obj[idx];
            if(idx=='birthday'){
                if(_prop!='' && _dateFormat(_prop)){
                    model[idx] = new Date(moment(_prop,"DD/MM/YYYY").format("YYYY-MM-DD"));
                } 
            }else if(typeof _prop != "object" && _prop!=''){
                model[idx] = _prop;
            }
        } 
    } 
};
mongoose.model('Client', ClientSchema);

module.exports = mongoose.model('Client');