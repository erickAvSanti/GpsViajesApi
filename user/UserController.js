var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var crypto = require('crypto');
var mongoose = require('mongoose');
var email_validator = require("email-validator");
var random = require("random-js")();

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());
router.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
    res.header("Access-Control-Allow-Headers", "X-API-KEY, X-ACCESS-TOKEN, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    //res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Content-Type', 'application/json');
    
    for(var idx in req.body){
        if(typeof req.body[idx] =="string"){
            req.body[idx] = req.body[idx].trim();
        }
    }
    for(var idx in req.params){
        if(typeof req.params[idx] == "string"){
            req.params[idx] = req.params[idx].trim();
        }
    }

    next();
});

var _verifyPermission = function(req,res,next){
    next();
    return;
    var method =req['method'];
    if(method=='GET'){

    }
    res.status(500).send({message:'cant-verify-permission'});
}

var _permission = function(req,res,next){   
    UserTokenController.verifyHeaderToken(req,res,function(pass,usertoken){
        if(pass){ 
            _verifyPermission(req,res,next);
        }else{
            res.end();
        }
    });
}

var User = require('./User');
var UserToken = require('./UserToken');
var UserTokenController = require('./UserTokenController');

/*// create a user a new user
var testUser = new User({
    _id: new mongoose.Types.ObjectId(),
    name: 'jmar',
    email: 'jmar777',
    password: 'Password123'
});

// save user to database
testUser.save(function(err) {
    if (err) throw err;

    // fetch user and test password verification
    User.findOne({ email: 'jmar777' }, function(err, user) {
        if (err) throw err;

        // test a matching password
        user.comparePassword('Password123', function(err, isMatch) {
            if (err) throw err;
            console.log('Password123:', isMatch); // -> Password123: true
        });

        // test a failing password
        user.comparePassword('123Password', function(err, isMatch) {
            if (err) throw err;
            console.log('123Password:', isMatch); // -> 123Password: false
        });
    });
});*/

router.validateForm = function (req,res,options){
    if(!options)options={};
    if(options.name===undefined)options.name = true;
    if(options.email===undefined)options.email = true;
    if(options.password===undefined)options.password = true;
    if(options.name){ 
        if(!req.body.name || req.body.name.trim()==''){
            return res.status(400).send({success:false,message:'name-undefined'});
        }
    }
    if(options.email){ 
        if(!req.body.email || req.body.email.trim()=='' || !email_validator.validate(req.body.email)){
            return res.status(400).send({success:false,message:'email-undefined'});
        }
    }
    if(options.password){ 
        if(!req.body.password || req.body.password.trim()=='' || req.body.password.trim().length<8){
            return res.status(400).send({success:false,message:'password-malformed'});
        }
    }
    return null;
}

// CREATES A NEW USER
router.post('/',_permission, function (req, res) {  
            var returned = router.validateForm(req,res);
            if(returned)return returned;

            User.findOne({ email: req.body.email }, function(err1, user) {
                if (err1){
                    return res.status(503).send({message:'server-error',success:false});
                }
                if(!user){
                    User.create({
                        _id:new mongoose.Types.ObjectId(),
                        name : req.body.name,
                        email : req.body.email,
                        password : req.body.password
                    }, 
                    function (err3, user_created) {
                        if (err3) return res.status(503).send({success:false,message:'server-error'});
                        if(user_created){ 
                            res.status(200).send({message:'user-created',success:true});
                        }else{
                            res.status(403).send({message:'user-not-created',success:false});
                        } 
                    });
                }else{
                    res.status(406).send({message:'email-already-exists',success:false});
                }
            });  
        
});
 
router.get('/',_permission, function (req, res) {  
            User.find({},"_id name email", function (err, users) {
                if (err) return res.status(500).send({success:true,message:'user-not-found'});
                res.status(200).send(users);
            }); 
});
 
router.get('/:id',_permission, function (req, res) { 
            User.findById(req.params.id, function (err1, user) {
                if (err1) return res.status(500).send({success:false,message:'user-not-found'});
                if (!user) return res.status(404).send({success:false,message:'user-not-found'});
                res.status(200).send(user);
            }); 
}); 
router.delete('/:id',_permission, function (req, res) { 
            User.findById(req.params.id, function (err, user) {
                if (err) return res.status(500).send({success:false,message:'server-error'});
                if(user){
                    user.remove(function(err2,_user){});
                }
                res.status(200).send({success:true,message:'user-deleted'});
            }); 
}); 
router.put('/:id',_permission, function (req, res) { 
            var returned = router.validateForm(req,res,{password:(req.body.password!='')});
            if(returned)return returned;

            User.findById(req.params.id, function(err1, user) {
                if (err1) return res.status(503).send({success:false,message:'server-error-0x0'});
                if(!user){
                    return res.status(404).send({success:false,message:'user-not-found'});
                }else{
                    user.name = req.body.name;
                    user.email = req.body.email;
                    if(req.body.password!='')user.password = req.body.password;
                    var _success = false;
                    user.save(function(err2,_user){
                        if(err2)return res.status(503).send({success:false,message:'server-error-0x1'});
                        if(_user)return res.status(200).send({success:true,message:'user-updated'}); 
                        return res.status(503).send({success:false,message:'server-error-0x2'}); 
                    });  
                } 
            });  
      
});



module.exports = router;