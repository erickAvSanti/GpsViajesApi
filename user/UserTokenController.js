var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var crypto = require('crypto');
var mongoose = require('mongoose');
var email_validator = require("email-validator");
var random = require("random-js")(); 
var User = require('./User');
var UserToken = require('./UserToken'); 
var useragent = require('express-useragent');

var _funcs = {
    get:function(token,cb){
        UserToken.findOne({token:token},function(err,userToken){
            cb(err,userToken);
        });
    },
    validate:function(token,cb){ 
        UserToken.findOne({token:token},function(err,userToken){  
            var isValid = (userToken ? (userToken.expire>Date.now()) : false);
            var timeDiff = ( userToken ? (userToken.expire-Date.now()):0);
            if(!isValid){
            	if(userToken){ 
	            	_funcs.removeToken(null,null,userToken.token,function(removed,_usertoken){
	            	});
            	}
            } 
            cb(
            	err,
            	userToken,
            	isValid,
            	timeDiff);
        });
    },    
	newToken:function(user,req,callback){ 
	    var source = req.headers['user-agent'],
	    ua = useragent.parse(source);
	    UserToken.create({
	        _id:new mongoose.Types.ObjectId(),
	        token: crypto.createHash('sha256').update(Date.now()+""+random.integer(1,10000)).digest('hex'),
	        created_by: user._id,
	        ua:JSON.stringify(ua),
	    },
	    function(err2,user_token_created){
	        if(user_token_created){
	            user.tokens.push(user_token_created._id);
	            user.save(function(err3){
	                callback(err3,user_token_created);
	            }); 
	        }else{
	            callback(err2,null);
	        }
	    }); 
	},
	removeToken:function(req,res,token,cb){ 
		UserToken.findOne({token:token}).populate('created_by').exec(function(err,usertoken){ 
			if(usertoken && usertoken.created_by){ 
				User.findById(usertoken.created_by._id,function(err2,user){ 
					if(user){
						var _arr = user.tokens;
						var _token_removed = false;
						for(var idx in _arr){
							if(_arr[idx]==usertoken._id.toString()){
								user.tokens.splice(idx,1);
								_token_removed = true;
								break;
							}
						}
						if(_token_removed){
							user.save(function(err3,_user){ 
							});
							usertoken.remove(function(err4,_usertoken){ 
							}); 
							return cb(true,usertoken);
						}
					}
					cb(false,usertoken);
				}); 
			}else{
				cb(true,usertoken);
			}
		});
	},
	verifyHeaderToken:function(req,res,cb){
		var token = req.headers['x-access-token']; 
  		if(!token){
  			res.status(406).send({message:'no-token-provided'});
  			return cb(false,null);
  		}
  		_funcs.validate(token,function(err5,usertoken,isValid,timeDiff){
    		if(!isValid){
    			res.status(404).send({message:'invalid-token',token:token});
    			return cb(false,usertoken);
    		} 
    		cb(true,usertoken); 
  		}); 
	},
};

module.exports = _funcs;