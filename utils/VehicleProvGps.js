var mongoose = require('mongoose');
const NameDuplicatedError = require('../errors/NameDuplicatedError');
var _props = {  
    _id: mongoose.Schema.Types.ObjectId,
    name: {type:String,required:true,unique:true},  
};
var VehicleProvGpsSchema = new mongoose.Schema(_props,
{ 
    timestamps: { 
        createdAt: 'created_at',
        updatedAt:'updated_at' 
    } 
});
VehicleProvGpsSchema.statics.props = _props;
VehicleProvGpsSchema.statics.makeFrom = function(obj){
    var _data = {};
    for(var idx in _props){ 
        if(idx in obj){
            var _prop = obj[idx];
            if(typeof _prop != "object" && _prop!=''){
                _data[idx] = _prop;
            }
        } 
    }
    return _data;
};
VehicleProvGpsSchema.statics.fillFrom = function(model,obj){ 
    for(var idx in _props){ 
        if(idx in obj){
            var _prop = obj[idx];
            if(typeof _prop != "object" && _prop!=''){
                model[idx] = _prop;
            }
        } 
    } 
}; 
const VehicleProvGps = mongoose.model('VehicleProvGps', VehicleProvGpsSchema);
VehicleProvGpsSchema.pre('save', function(next) { 
    var _this = this;
    this.name = this.name.toUpperCase();
    var re = new RegExp(`^${this.name}$`,'i');
    VehicleProvGps.find({name:re},function(err,docs){ 
        if(docs){
            if(docs.length>0){
                var find = false;
                for(var idx in docs){
                    var doc = docs[idx]; 
                    if(doc._id.toString() == _this._id.toString()){
                        find = true;
                        break;
                    }
                }
                if(find){
                    return next();
                } 
                next(new NameDuplicatedError({attr:_this.name}));
            }else{
                next();
            }
        }
    });
    
}); 

module.exports = VehicleProvGps;