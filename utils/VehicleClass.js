var mongoose = require('mongoose');
const NameDuplicatedError = require('../errors/NameDuplicatedError');
var _props = {  
    _id: mongoose.Schema.Types.ObjectId,
    name: {type:String,required:true,unique:true},  
};
var VehicleClassSchema = new mongoose.Schema(_props,
{ 
    timestamps: { 
        createdAt: 'created_at',
        updatedAt:'updated_at' 
    } 
});
VehicleClassSchema.statics.props = _props;
VehicleClassSchema.statics.makeFrom = function(obj){
    var _data = {};
    for(var idx in _props){ 
        if(idx in obj){
            var _prop = obj[idx];
            if(typeof _prop != "object" && _prop!=''){
                _data[idx] = _prop;
            }
        } 
    }
    return _data;
};
VehicleClassSchema.statics.fillFrom = function(model,obj){ 
    for(var idx in _props){ 
        if(idx in obj){
            var _prop = obj[idx];
            if(typeof _prop != "object" && _prop!=''){
                model[idx] = _prop;
            }
        } 
    } 
}; 
const VehicleClass = mongoose.model('VehicleClass', VehicleClassSchema);
VehicleClassSchema.pre('save', function(next) { 
    var _this = this;
    this.name = this.name.toUpperCase();
    var re = new RegExp(`^${this.name}$`,'i');
    VehicleClass.find({name:re},function(err,docs){ 
        if(docs){
            if(docs.length>0){
                var find = false;
                for(var idx in docs){
                    var doc = docs[idx]; 
                    if(doc._id.toString() == _this._id.toString()){
                        find = true;
                        break;
                    }
                }
                if(find){
                    return next();
                } 
                next(new NameDuplicatedError({attr:_this.name}));
            }else{
                next();
            }
        }
    });
    
}); 

module.exports = VehicleClass;